//
//  Constants.swift
//  ColorMemory
//
//  Created by Sagar Verma on 5/10/17.
//  Copyright © 2017 Sagar Verma. All rights reserved.
//

import Foundation
import UIKit

enum GameLevel:Int {
    case easy = 0
    case hard = 1
}

struct Constants {
    
    let GRID_SIZE:CGFloat = 4
    let SPACING:CGFloat = 2.0
    let ICON_RASIO:CGFloat = 1.2
    let WIN_POINTS:Int = 2
    let LOOSE_POINTS:Int = -1
    let HIGH_SCORE:String = "HIGH_SCORE"

}
