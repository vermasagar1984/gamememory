//
//  GameAnimations.swift
//  ColorMemory
//
//  Created by Sagar Verma on 10/05/17.
//  Copyright © 2017 Sagar Verma. All rights reserved.
//

import Foundation
import UIKit

extension GameBoardViewController{

    func flipLeftAnimation(cell:GameBoardCell, randomNumber:Int ){
        UIView.transition(with: cell.icon, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: {
            cell.icon.index = randomNumber
            cell.icon.image = UIImage(named: "card\(randomNumber).png")
        }, completion: { (finish:Bool) in
        })
    }
    
    func flipRightAnimation(cell:GameBoardCell){
        UIView.transition(with: cell.icon, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromRight, animations: {
            cell.icon.image = UIImage(named:"card_bg.png")
        }, completion: { (finish:Bool) in
            
        })
    }
}
