//
//  GameBoardCell.swift
//  ColorMemory
//
//  Created by Sagar Verma on 09/05/17.
//  Copyright © 2017 Sagar Verma. All rights reserved.
//

import Foundation
import UIKit


class GameBoardCell: UICollectionViewCell {

    @IBOutlet weak var icon: GameUIImageView!
    
}
