//
//  ViewController.swift
//  ColorMemory
//
//  Created by Sagar Verma on 09/05/17.
//  Copyright © 2017 Sagar Verma. All rights reserved.
//

import UIKit


class GameBoardViewController: UIViewController {

    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var gameBoardView: UICollectionView!
    var preDefinedIndexes:[Int] = []
    var gameLevel:GameLevel! = .none
    var screenSize: CGRect!
    var playerModel:GamePlayerModel = GamePlayerModel()
    let constants = Constants()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        screenSize = UIScreen.main.bounds
        let iconWidth:CGFloat = CGFloat(screenSize.width/constants.GRID_SIZE - 2*constants.SPACING)
        let iconHeight:CGFloat = CGFloat(iconWidth*constants.ICON_RASIO)
        
        // Do any additional setup after loading the view, typically from a nib
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        print(iconWidth)
        print(iconHeight)
        print("GameLevel" + "\(self.gameLevel)")
        
        layout.itemSize = CGSize(width: iconWidth, height: iconHeight)
        layout.minimumInteritemSpacing = constants.SPACING
        layout.minimumLineSpacing = constants.SPACING
        gameBoardView!.collectionViewLayout = layout
        
        var collectionRect:CGRect = self.gameBoardView.frame
        collectionRect.size.height = iconWidth*(constants.ICON_RASIO)*(2*constants.SPACING)+(3*constants.SPACING)
        collectionRect.origin.y = (screenSize.height - collectionRect.size.height)/2
        self.gameBoardView.frame = collectionRect
        
        if self.gameLevel == .easy {
            createPreDefinedIndexesArray()
        }
        
        setTextInPointsLabel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "highscore" {
//            let highScoreController = segue.destination as? GameHighScoreViewController
//        }
    }
}

//MARK: - UICollectionViewDataSource

extension GameBoardViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GameBoardCell
        cell.icon.image = UIImage(named:"card_bg.png")
        cell.icon.didClickImageView = {()->Void in
            self.playerModel.selectedCells.append(cell)
            cell.isUserInteractionEnabled = false
            if cell.icon.isIconShown {
                let randomNumber = self.generateRandomNumberAsPerGameLevel(row: indexPath.row)
                self.flipLeftAnimation(cell: cell, randomNumber: randomNumber)
            }else{
                cell.icon.index = -1
                self.flipRightAnimation(cell: cell)
            }
            self.checkForMatchIcons()
        }
        return cell
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}

//MARK: - GameBoardDelegates

extension GameBoardViewController: GameBoardDelegates {
    
    func setTextInPointsLabel() {
        self.pointsLabel.text = "Points: " + "\(self.playerModel.palyerTotalPoints)"
    }
    
    func checkForMatchIcons() {
        
        if self.playerModel.selectedCells.count == 2 {
            self.view.isUserInteractionEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] _ in
                // your code here
                self?.view.isUserInteractionEnabled = true
                let firstCell:GameBoardCell = self!.playerModel.selectedCells[0]
                let secondCell:GameBoardCell = self!.playerModel.selectedCells[1]
                
                if firstCell.icon.index == secondCell.icon.index {
                    print("win")
                    self?.playerModel.palyerTotalPoints += (self?.constants.WIN_POINTS)!
                    self?.playerModel.matchedIconsIndexes.append(firstCell.icon.index)
                    for cell in (self?.playerModel.selectedCells)!{
                        cell.isUserInteractionEnabled = false
                        cell.icon.isHidden = true
                    }
                }else{
                    self?.playerModel.palyerTotalPoints += (self?.constants.LOOSE_POINTS)!
                    for cell in (self?.playerModel.selectedCells)!{
                        cell.isUserInteractionEnabled = true
                        cell.icon.index = -1
                        cell.icon.isIconShown = false
                        self?.flipRightAnimation(cell: cell)
                    }
                }
                self?.playerModel.selectedCells.removeAll()
                self?.setTextInPointsLabel()
                self?.checkForWin()
            }
        }
    }
    
    func createPreDefinedIndexesArray(){
        if self.gameLevel == .easy {
            self.preDefinedIndexes = [1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8]
            var results = [Int]()
            while self.preDefinedIndexes.count > 0 {
                let indexOfIndexes = Int(arc4random_uniform(UInt32(self.preDefinedIndexes.count)))
                let index = self.preDefinedIndexes[indexOfIndexes]
                results.append(index)
                self.preDefinedIndexes.remove(at: indexOfIndexes)
            }
            self.preDefinedIndexes = results
        }
    }
    
    func generateRandomNumberAsPerGameLevel(row:Int) -> Int {
        
        var randomNumber = -1
        if self.gameLevel == .easy {
            randomNumber = self.preDefinedIndexes[row]
        }else{
            repeat{
                randomNumber = Int(arc4random_uniform(8) + 1)
            }while(self.playerModel.matchedIconsIndexes.contains(randomNumber))
        }
        return randomNumber
    }
    
    func checkForWin() {
        
        if self.playerModel.matchedIconsIndexes.count >= 8 {
            let alertController = UIAlertController(title: "Your Score :\(self.playerModel.palyerTotalPoints)", message: "Please enter your name to save score.", preferredStyle: .alert)
            
            let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
                alert -> Void in
                
                let firstTextField = alertController.textFields![0] as UITextField
                
                if firstTextField.text == nil || firstTextField.text?.characters.count == 0 {
                    self.checkForWin()
                }else{
                    self.playerModel.playerName = firstTextField.text!
                    let fileManager = FileManager.default
                    
                    let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
                    let path = documentDirectory.appending("/highscore.plist")
                    
                    if(!fileManager.fileExists(atPath: path)){
                        print(path)
                        let data : [String: Any] = [
                            self.playerModel.playerName: self.playerModel.palyerTotalPoints
                            // any other key values
                        ]
                        let someData = NSDictionary(dictionary: data)
                        someData.write(toFile: path, atomically: true)
                    }else{
                        let savedAllUserData = (NSDictionary(contentsOfFile: path))?.mutableCopy() as! NSMutableDictionary
                        let savedScore = savedAllUserData[self.playerModel.playerName]
                        if let score:Int = savedScore as? Int{
                            if score < self.playerModel.palyerTotalPoints {
                                savedAllUserData[self.playerModel.playerName] = self.playerModel.palyerTotalPoints
                            }
                        }else{
                            savedAllUserData[self.playerModel.playerName] = self.playerModel.palyerTotalPoints
                        }
                        savedAllUserData.write(toFile: path, atomically: true)
                    }
                    let save = NSDictionary(contentsOfFile: path)
                    print(save ?? "scvsd")
                }
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                (action : UIAlertAction!) -> Void in
            })
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Enter Name"
            }
            
            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
