//
//  GamePlayerModel.swift
//  ColorMemory
//
//  Created by Sagar Verma on 5/10/17.
//  Copyright © 2017 Sagar Verma. All rights reserved.
//

import Foundation
import UIKit

protocol GameBoardDelegates {
    func createPreDefinedIndexesArray()
    func setTextInPointsLabel()
    func checkForMatchIcons()
    func generateRandomNumberAsPerGameLevel(row:Int) -> Int
    func checkForWin()
}


struct GamePlayerModel {
    
    var playerName:String = "none"
    var palyerTotalPoints:Int = 0
    var matchedIconsIndexes:[Int] = []
    var selectedCells:[GameBoardCell]! = []

}
