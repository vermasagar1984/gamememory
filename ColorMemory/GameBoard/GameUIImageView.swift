//
//  GameBoardCell.swift
//  ColorMemory
//
//  Created by Sagar Verma on 09/05/17.
//  Copyright © 2017 Sagar Verma. All rights reserved.
//


import UIKit

@IBDesignable class GameUIImageView: UIImageView {
    
    @IBInspectable var enableTapGestue: Bool = false
    var isIconShown:Bool = false
    var index:Int = -1
    
    var didClickImageView : (()->Void)!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        if self.enableTapGestue == true {
            self.addTapGestureOnImageView(imageView: self)
        }
    }
    
    func addTapGestureOnImageView(imageView:UIImageView){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
    
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if self.enableTapGestue == true{
            if let closure = self.didClickImageView{
                isIconShown = !isIconShown
                closure()
            }
        }
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
