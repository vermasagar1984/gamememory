//
//  GameHighScoreViewController.swift
//  ColorMemory
//
//  Created by Sagar Verma on 5/11/17.
//  Copyright © 2017 Sagar Verma. All rights reserved.
//

import Foundation
import UIKit

class GameHighScoreViewController: UIViewController {

    @IBOutlet var highScoreTableView: UIView!
    var userTupleArray = [(String,Int)]()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let fileManager = FileManager.default
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = documentDirectory.appending("/highscore.plist")
        if(fileManager.fileExists(atPath: path)){
            print(path)
            if let allSavedUserData:[String:Int] = (NSDictionary(contentsOfFile: path) as? [String : Int])!{
                userTupleArray = allSavedUserData.sorted{ $0.value > $1.value }
            }
        }
    }
}


extension GameHighScoreViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return userTupleArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath ) as! GameHighScoreCell
        let (name,score) = userTupleArray[indexPath.row]
        cell.nameLabel.text = name
        cell.scoreLabel.text = String(describing: score)
        cell.rankLabel.text = String(describing: (indexPath.row+1))
        return cell
    }
}

