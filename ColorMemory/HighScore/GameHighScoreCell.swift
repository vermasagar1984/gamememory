//
//  GameHighScoreModel.swift
//  ColorMemory
//
//  Created by Sagar Verma on 5/11/17.
//  Copyright © 2017 Sagar Verma. All rights reserved.
//

import Foundation
import UIKit

class GameHighScoreCell:UITableViewCell {
    

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!
}
