//
//  HomeScreenViewController.swift
//  ColorMemory
//
//  Created by Sagar Verma on 5/10/17.
//  Copyright © 2017 Sagar Verma. All rights reserved.
//

import Foundation
import UIKit

class HomeScreenViewController: UIViewController {
    
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    var selectedSegmentedValue:Int! = 0
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func segmentedValueChangeAction(_ sender: Any) {
        self.selectedSegmentedValue = self.segmentedControl.selectedSegmentIndex
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gamestart" {
            let gameBoardController = segue.destination as? GameBoardViewController
            gameBoardController?.gameLevel = self.selectedSegmentedValue == 1 ? .hard : .easy
        }
    }
}
